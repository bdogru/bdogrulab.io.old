---
title: 'cubox nasıl kurulur?'
date: 2019-05-06 13:07:17
type: post
blog: true
excerpt: 'I was reading my daily Quora digest this morning and the following post: \"What did you do to make your software career better?\" To my surprise and disappointment, one of the most upvoted answers was "to quit ... [because of] cheap outsourcing." And while outsourcing has had an impact on the job market, this answer is complete nonsense'
---

![olidrun-cubox-i4pro-development-platform-product-tour-5](https://i0.wp.com/www.bahadirdogru.com/wp-content/uploads/2015/02/olidrun-cubox-i4pro-development-platform-product-tour-5-300x169.jpg?resize=300%2C169)](https://i1.wp.com/www.bahadirdogru.com/wp-content/uploads/2015/02/olidrun-cubox-i4pro-development-platform-product-tour-5.jpg)Cubox bir mini bilgisayardır. [SolidRun](http://www.solid-run.com/) şirketi tarafından Arm tabanlı işlemcisi ile 7/24 çalışıcak şekilde tasarlanmıştır. Cubox’un önemi portatif ve şık tasarımından gelir. 5*5*5 cm ebatlarındaki şık tasarımlı kutusu ile raspberry pi ile kıyasla daha mantıklı bir çözüm olabilir developer’lar için. Estetik tasarımı ile beraber birçok bağlantı türünü de destekler cubox.

 

 

 

- 1. Optik S/PDIF ses çıkışı[![cubox-i-ports](https://i1.wp.com/www.bahadirdogru.com/wp-content/uploads/2015/02/cubox-i-ports-300x226.png?resize=300%2C226)](https://i2.wp.com/www.bahadirdogru.com/wp-content/uploads/2015/02/cubox-i-ports.png)
- 2. Elektrik Bağlantısı
- 3. Ethernet Bağlantısı
- 4. HDMI Kanalı
- 5. Micro SD Kart girişi
- 6. USB Girişi
- 7. USB Girişi
- 8. eSATA girişi ( Teoride 300 MB/saniye )
- 9. Micro USB girişi

Cubox çeşitli işletim sistemleri ile kullanılabilir. Kendisi paketinde Android ile beraber gelmektedir. Benim gibi televizyonunuzu media server’a dönüştürmek için düşünürseniz artık cuboxtv ismiyle direkt olarak [openelec.tv](http://www.bahadirdogru.com/wp-content/uploads/2015/02/openelec.tv) yazılımı ile beraber geliyor. Yani içerisinde [Kodi](http://www.bahadirdogru.com/wp-content/uploads/2015/02/kodi.tv) projesi olan bir televizyona sahip oluyorsunuz. Bunlar temel olarak gelen ve karşınıza çıkan seçenekler. Şimdi size mantığından bahsedeyim.

Bu küçük bilgisayar Arm tabanlı işlemcisi ile SD-Kart yuvasına takılan tüm sistemleri çalıştırabilir. İşletim sisteminin U-Boot standardına uygun ve cubox driverlarına sahip olması yeterlidir. Dolayısıyla birkaç Sd-Kartı tak-çıkar yaparak çeşitli sistemleri çalıştırmanız mümkün. Gui (Grafik arayüzü) itibariyle Kodi ve Android haricinde pek verimli bulmasamda bu cihazı. Cihaz ve konfigurasyonlarını düşününce ihtimaller çok ilginç olabiliyor. Bunlardan birkaçına göz atalım;

Cubox sırasıyla;

- Android
- ArchLinux
- Debian
- Fedora
- GeeXboX
- Gentoo
- OpenADK
- OpenELEC
- OpenSUSE
- RedSleeve
- Ubuntu
- Volumio
- Voyage MuBox
- XBian
- Yocto

Projelerini çalıştırabilmekte. Misal benim kullanımım üzerinden örnek vericek olursam Debian işletim sistemi üzerinde Nginx webserver yaparak kendi local web hizmetlerimi kurabiliyorum. Bunlara Transmission gibi open source projeleri de dahil ettiğimde; Torrent dosyalarımı takip ettiğim ve uzaktan erişim yapabildiğim bir server haline gelebiliyor.


Almayı düşünenler için fiyat tablosu;

CuBox i2 eX ( 2 Çekirdek işlemci, 1gb ram) : 130$

CuBox i4Pro ( 4 Çekirdek işlemci, 2gb ram): 150$

CuBoxTV (4 Çekirdek işlemci, 1gb ram): 120$

Dönem dönem şirket kampanyalar yaparak daha ucuza erişilebilir hale getiriyor, tabi takip etmek lazım.
