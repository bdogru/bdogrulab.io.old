# Bahadır Doğru

originaly forked from bencodezen's blog.

## Dependencies

- [VuePress](https://vuepress.vuejs.org/) - Static site generator based with a [Vue.js](https://www.vuejs.org) powered theming system

## Setup

### Setup Your Own Version

If you like my blog and would like to use the engine powering it, check out bencodezen's vuepress blog Boilerplate: [VuePress Blog Boilerplate](https://github.com/bencodezen/vuepress-blog-boilerplate).

### Local Setup

1. Fork, clone or download this project
1. Install dependencies: `npm install`
1. Preview site with: `npm run dev`

## Get in Touch

Feel free to reach out to me via the following:

- [Twitter](http://www.twitter.com/bhdrd)
- 
## To Do
- Transfer to jekyll with theme for better view.
