# About Me

## English
Hi, my name is Bahadır Doğru. I was born in 1992 in Istanbul. I studied highschool at Çınar Collage and then Marmara University Business Administration in german language.
My curiosity to understand the things background has made me more inclined to philosophy and technology. In terms of usefulness, I left the philosophy to devote myself to learning about the technical works.
You will find in this blog how i think, develop and research. Shortly, you will find my Know-Hows.

After my adventures with Gwbasic, which my father taught me, I started to understand VB 6.0 and .net. As a microsoft-guy and a successful computer user, I noticed later that all these were just Screen Savers.
In 2010 I met my brother [Furkan Tektaş](http://www.furkantektas.com) and with his help I step into the world of Linux.
If, as an Ottoman grandson, I would like to describe myself as the so-called in ottoman tradition "Getting Feyz(Soul)";
The soul of Open-Source was taken by my brother Furkan Tektaş and he took from his teacher Hasan Tuncer. :)

Now I can say that I liked the Debian Social Contract as a debian user. Here you can review: [https://www.debian.org/social_contract](https://www.debian.org/social_contract)

I am grateful to [Emrah](http://www.emrah.com) brother for his posts about debian and some stuffs.
Having found my explanations on the internet at a time of very late night after my research has been difficult to understand; It was an ”incredible experience“.

Nowadays i work at [Damla Yayınevi](http://www.damlayayinevi.com.tr) while I continue to improve myself.

I would also like to thank Mehmet Kacar, who is a supporter of network information, hardware and system architecture. I learned a lot from him about his experience, IT discipline and control.

This is it for now, i think.
With the hope about evolving over time and write more beautiful things..

## Türkçe
Merhaba ben Bahadır Doğru. 1992 yılında istanbulda doğdum. Lisede özel çınar Koleji’nde üniversitede Marmara üniversitesi Almanca İşletmeyi bölümünde okudum.
Hep “arka planı” anlama merakım benim felsefeye ve teknolojiye yatkın olmamı sağladı. İşe yararlılık açısından felsefeyi bırakıp kendimi teknik işleri öğrenmeye adadım.
Bu blogda bunları yaparken neyi nasıl düşündüğümü, geliştirdiğimi ve araştırdığımı. Kısacası “Know-How” larımı bulacaksınız.

Babamın öğrettiği Gwbasic ile bilgisayarı anlamaya başladığım serüvenim sonrasında VB 6.0 .Net öğrenmeye ve araştırmaya sevk etti beni.
Bir microsoftçu ve başarılı bir bilgisayar kullanıcısı olarak sonradan fark ettim ki aslında tüm bunlar sadece “Ekran Koruyucu” imiş.
2010 Yılında [Furkan Tektaş](http://www.furkantektas.com) abimle tanışmam ile hep merak ettiğim ama cesaret edemediğim Linux dünyasına adım attım.
Eğer bir Osmanlı torunu olarak kendimi kadim gelenekte olduğu gibi “feyz almak” diye tabir edilen anlayış ile tarif etmem gerekirse;
Open-Source ruhunu Abim Furkan Tektaştan o da hocası Hasan Tuncer’den almıştır. :)

Şimdiler de debian kullanan biri olarak debian Sosyal Sözleşmesini çok sevdim diyebilirim. Buradan inceleyebilirsiniz:  [https://www.debian.org/social_contract](https://www.debian.org/social_contract)
Debian ve kurulumu, üzerinde çalıştırılan uygulamalar ve referans alınacak bir örnek olarak notlarını takip ettiğim ve sık sık internet sitesini takip ettiğim [emrah](http://www.emrah.com) abi’ye de şükranlarımı borç bilirim. Araştırmalarımın neticesiz kalması yada anlamakta zorlanmam sonrasında internet sitesinde gecenin bir vakti aradığım açıklamaları bulmuş olmak; “Anlatılmaz yaşanır” bir durumdu.

Şimdilerde [Damla Yayınevi](http://www.damlayayinevi.com.tr)‘nin IT işlerini yaparken bir yandan kendimi geliştirmeye devam ediyorum.

Network bilgileri, donanım ve sistem mimari bilgileri ile bana yardımcı olan, destek olan Mehmet Kaçar abime de teşekkürü borç bilirim. Kendisinden tecrübeyi, IT disiplini ve kontrolü hakkında çok şey ögrendim.

Şimdilik bu kadar sanırım, zamanla tekamül edip daha güzel şeyler yazabilmek ümidiyle..

## Deutsch
Es wird geschrieben, wenn ich gelegenheit finde.