---
home: true
heroImage: /bahadirdogru-logo.jpg
heroText: Bahadır Doğru
tagline: Hi, welcome my page!
taglineAlt: There is some notes about tech :)
actionText: Read the latest →
actionLink: /blog/
features:
- title: Consulting
  details: I provide web development services like magento. If you help with magento, debian based servers or tableau you can contact me. 
- title: Notes
  details: I'm collecting the most useful code snippets or some shell scripts. On various topics on web development and best practices. I'm always creating new topics as well, so please reach out if you'd like to me discuss!
- title: Bio
  details: I studied Business Administration in german language, but professionaly i am working in the IT industry.
footer: © Bahadır Doğru 2019. Made with VuePress.
---
